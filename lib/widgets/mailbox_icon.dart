import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/material.dart';

mailboxIcon(Mailbox mailbox) {
  IconData iconData = Icons.folder;
  if (mailbox.isArchive) iconData = Icons.archive;
  if (mailbox.isDrafts) iconData = Icons.architecture;
  if (mailbox.isInbox) iconData = Icons.inbox;
  if (mailbox.isJunk) iconData = Icons.warning;
  if (mailbox.isSent) iconData = Icons.send_and_archive;
  if (mailbox.isTrash) iconData = Icons.delete;
  return Icon(iconData);
}
