import 'package:flutter/material.dart';

class ContextMenu extends StatelessWidget {
  final Offset position;
  final List<Widget> children;

  const ContextMenu({Key key, this.position, this.children}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = (children.length * 48 + 2 * 16).toDouble();
    if (height > MediaQuery.of(context).size.height)
      height = MediaQuery.of(context).size.height;

    double paddingLeft = position.dx;
    double paddingTop = position.dy;
    double paddingRight = MediaQuery.of(context).size.width - position.dx - 320;
    if (paddingRight < 0) {
      paddingLeft += paddingRight;
      paddingRight = 0;
    }
    double paddingBottom =
        MediaQuery.of(context).size.height - position.dy - height;
    if (paddingBottom < 0) {
      paddingTop += paddingBottom;
      paddingBottom = 0;
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(
        paddingLeft,
        paddingTop,
        paddingRight,
        paddingBottom,
      ),
      child: SizedBox.shrink(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Material(
            elevation: 8,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 16),
              child: ListView(
                  primary: false, shrinkWrap: true, children: children),
            ),
          ),
        ),
      ),
    );
  }
}
