import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MainSecondary extends StatefulWidget {
  final Widget main;
  final Widget secondary;

  const MainSecondary({Key key, this.main, this.secondary}) : super(key: key);
  @override
  _MainSecondaryState createState() => _MainSecondaryState();
}

class _MainSecondaryState extends State<MainSecondary> {
  PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    bool isSmallScreen = MediaQuery.of(context).size.width < 1024;
    return isSmallScreen
        ? PageView(
            children: [
              GestureDetector(
                child: widget.main,
                behavior: HitTestBehavior.deferToChild,
                onTapDown: (details) => _controller.animateToPage(1,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeInOut),
              ),
              widget.secondary
            ],
            controller: _controller,
          )
        : Row(
            children:
                [widget.main, if (widget.secondary != null) widget.secondary]
                    .map((e) => Flexible(
                          child: e,
                          fit: FlexFit.tight,
                        ))
                    .toList(),
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            /*scrollDirection: Axis.horizontal,
            primary: false,
            shrinkWrap: true,*/
          );
  }
}
