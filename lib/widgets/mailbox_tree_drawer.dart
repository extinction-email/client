import 'package:client/pages/login_page.dart';
import 'package:client/widgets/mailbox_icon.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_gravatar/simple_gravatar.dart';

class MailboxTreeDrawer extends StatefulWidget {
  static MailboxTreeDrawer current(
      {Key key,
      mailClients,
      mailboxes,
      onMailboxChange,
      onMailClientChange,
      initialMailbox,
      initialMailClient}) {
    if (mailClients != null && mailboxes != null)
      _saved = MailboxTreeDrawer(
          key: key,
          mailClients: mailClients,
          mailboxes: mailboxes,
          onMailboxChange: onMailboxChange,
          onMailClientChange: onMailClientChange,
          initialMailbox: initialMailbox,
          initialMailClient: initialMailClient);
    else if (_saved == null) _saved = MailboxTreeDrawer();
    return _saved;
  }

  static MailboxTreeDrawer _saved;
  final List<MailClient> mailClients;
  final Map<String, Tree<Mailbox>> mailboxes;
  final Function(Mailbox) onMailboxChange;
  final Function(int) onMailClientChange;
  final Mailbox initialMailbox;
  final int initialMailClient;

  const MailboxTreeDrawer(
      {Key key,
      this.mailClients,
      this.mailboxes,
      this.onMailboxChange,
      this.onMailClientChange,
      this.initialMailbox,
      this.initialMailClient = 0})
      : super(key: key);

  @override
  _MailboxTreeDrawerState createState() => _MailboxTreeDrawerState();
}

class _MailboxTreeDrawerState extends State<MailboxTreeDrawer> {
  @override
  Widget build(BuildContext context) {
    return widget.mailboxes == null || widget.mailboxes.isEmpty
        ? Center(child: CircularProgressIndicator())
        : Drawer(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  UserAccountsDrawerHeader(
                    accountEmail: Text(
                        widget.mailClients.first.account.email.toLowerCase()),
                    accountName: Text('Extinction.Email'),
                    currentAccountPicture: Image.network(Gravatar(widget
                            .mailClients.first.account.email
                            .toLowerCase())
                        .imageUrl(
                            size: 256, defaultImage: GravatarImage.identicon)),
                    onDetailsPressed: () {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                            'Settings are unfortunately not implemented yet.'),
                      ));
                    },
                  ),
                ]
                  ..addAll(widget.mailboxes[widget
                          .mailClients[widget.initialMailClient].account.name]
                      .flatten((element) => true)
                      .map((e) => ListTile(
                            tileColor: widget.initialMailbox == e
                                ? Theme.of(context).primaryColor.withAlpha(128)
                                : null,
                            title: Text(e.name),
                            leading: mailboxIcon(e),
                            onTap: () => widget.onMailboxChange(e),
                          )))
                  ..add(Divider())
                  ..add(
                    FutureBuilder(
                        future: PackageInfo.fromPlatform(),
                        builder:
                            (context, AsyncSnapshot<PackageInfo> snapshot) {
                          String version = 'unknown';
                          String build = 'unknown';
                          if (snapshot.hasData) {
                            version = snapshot.data.version;
                          }
                          return ListTile(
                            title: Text('About'),
                            leading: Icon(Icons.info),
                            onTap: () => showAboutDialog(
                                context: context,
                                applicationName: 'extinction.email',
                                applicationVersion:
                                    'Version $version build $build',
                                applicationLegalese:
                                    'Copyright © 2020 TestApp.schule',
                                applicationIcon: Image.asset(
                                  'assets/icons/icon-white-android.png',
                                  scale: 4,
                                )),
                          );
                        }),
                  )
                  ..add(ListTile(
                    title: Text('Logout'),
                    leading: Icon(Icons.settings_power),
                    onTap: () async {
                      final prefs = await SharedPreferences.getInstance();
                      await prefs.clear();
                      Navigator.of(context)
                          .pushReplacementNamed(LoginPage.routeName);
                    },
                  )),
              ),
            ),
          );
  }
}
