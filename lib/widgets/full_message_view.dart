import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:client/widgets/html_message.dart';
import 'package:client/widgets/message_attachment_tile.dart';
import 'package:client/widgets/plain_text_message.dart';
import 'package:enough_mail/enough_mail.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:link_previewer/link_previewer.dart';
import 'package:openpgp/openpgp.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_gravatar/simple_gravatar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class FullMessageView extends StatefulWidget {
  final MimeMessage message;

  const FullMessageView({Key key, this.message}) : super(key: key);

  @override
  _FullMessageViewState createState() => _FullMessageViewState();
}

class _FullMessageViewState extends State<FullMessageView> {
  PGPMetadata pgpMetadata;
  bool initialized = false;

  MimeMessage message;

  @override
  void didUpdateWidget(covariant FullMessageView oldWidget) {
    if (oldWidget.message != widget.message) {
      setState(() {
        initialized = false;
      });
      initialization();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    initialization();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!initialized) return Center(child: CircularProgressIndicator());
    if (Theme.of(context).platform == TargetPlatform.android)
      WebView.platform = SurfaceAndroidWebView();

    //detectPGPMessage();
    detectPGPSignature();
    /*try {
      //print(message.decodeTextPlainPart());
    } catch (e) {}
    message.parts.forEach((element) {
      print(element.getHeaderContentType().rawValue);
    });*/

    String body;
    if (!isHtmlMessage(message) && message.decodeTextPlainPart() != null)
      try {
        body = Utf8Codec().decode(
            Uint8List.fromList(message.decodeTextPlainPart().codeUnits));
      } catch (e) {
        body = message.decodeTextPlainPart();
      }

    return ListView(
      children: [
        ListTile(
          title: SelectableText(message.decodeSubject()),
          leading: Container(
            constraints: BoxConstraints.lerp(
                BoxConstraints(maxWidth: 256),
                BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 5),
                1),
            child: Wrap(
              children: message
                  .decodeSender()
                  .map((e) => Tooltip(
                      message: e.email,
                      child: GestureDetector(
                        onTap: () {
                          Clipboard.setData(ClipboardData(text: e.toString()));
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text('Copied email address.'),
                          ));
                        },
                        child: Chip(
                          avatar: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.network(Gravatar(e.email.toLowerCase())
                                .imageUrl(
                                    size: 128,
                                    defaultImage: GravatarImage.identicon)),
                          ),
                          label: Text(e.personalName ?? e.email),
                        ),
                      )))
                  .toList(),
            ),
          ),
          trailing: Container(
              constraints: BoxConstraints.lerp(
                  BoxConstraints(maxWidth: 64),
                  BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width / 12),
                  0.5),
              child: Text(message.decodeDate().toString())),
        ),
        ButtonBar(
          children: [
            IconButton(
                icon: Icon(Icons.print),
                tooltip: 'Print',
                onPressed: () async {
                  // printing is not yet supported on Linux
                  if (!kIsWeb &&
                      Theme.of(context).platform == TargetPlatform.linux) {
                    try {
                      final url =
                          'data:text/${isHtmlMessage(message) ? 'html' : 'text'};base64,${base64.encode(Uint8List.fromList((message.decodeTextHtmlPart() ?? message.decodeTextPlainPart()).codeUnits))}';
                      Clipboard.setData(ClipboardData(text: url));
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                            'Unfortunately, you need to print from your browser.'),
                        action: SnackBarAction(
                          label: 'See why',
                          onPressed: () => launch(
                              'https://github.com/flutter/flutter/issues/41724'),
                        ),
                      ));
                      final result = await Process.run('xdg-open', [url]);
                      if (result.stderr.toString().isNotEmpty) throw Error();
                    } catch (e) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text('Error printing the file.')));
                    }
                    return;
                  }
                  await Printing.layoutPdf(
                      format: PdfPageFormat.a4,
                      onLayout: (f) async => await Printing.convertHtml(
                          format: f,
                          html: isHtmlMessage(message)
                              ? message.decodeTextHtmlPart() ??
                                  message.decodeTextPlainPart()
                              : body.replaceAll('\n', '<br/>\n')),
                      name: message.decodeSubject() + '.pdf');
                }),
          ],
        ),
        if (pgpMetadata.isEncrypted)
          ListTile(
            leading: Icon(Icons.security, color: Colors.green),
            title: Text('Message is PGP encrypted'),
          ),
        if (message.hasAttachments())
          MessageAttachmentTile(parts: message.parts),
        Padding(
          padding: const EdgeInsets.all(16),
          child: isHtmlMessage(message)
              ? HtmlMessage(
                  message.decodeTextHtmlPart() ?? message.decodeTextPlainPart())
              : PlainTextMessage(message: body),
        ),
      ],
    );
  }

  Future initialization() async {
    pgpMetadata = PGPMetadata();
    message = MimeMessage();
    await detectPGPMessage();
    setState(() {
      initialized = true;
    });
  }

  Future<void> detectPGPMessage() async {
    if (widget.message
        .decodeHeaderValue('Content-Type')
        .contains('pgp-encrypted')) {
      pgpMetadata.isEncrypted = true;
      print('Encrypted!');
      //var bytesSample = Uint8List.fromList('data'.codeUnits);
      final prefs = await SharedPreferences.getInstance();
      final keys = prefs.getString('private_keys');
      String privateKey = jsonDecode(keys)[0]['key'];
      String password = jsonDecode(keys)[0]['password'];
      try {
        var result = await OpenPGP.decrypt(
            widget.message.decodeContentText(), privateKey, password);
        pgpMetadata.hasSecretKey = true;
        message.bodyRaw = result;
      } catch (e) {}
      Clipboard.setData(ClipboardData(text: message.decodeContentText()));
      //print(message.decodeContentText());
      //if (message.hasAttachmentsOrInlineNonTextualParts())
      //message.headers.forEach((element) {
      //  if (element.toString().contains('attach')) print(element);
      //});
    } else
      message = widget.message;
  }

  void detectPGPSignature() {
    //print(widget.message.headerRaw);
    Clipboard.setData(ClipboardData(text: message.bodyRaw));
    //message.headers.forEach((element) {
    //  if (element.toString().contains('attach')) print(element);
    //});
  }
}

const String pgpMessageRegex =
    r'-----BEGIN PGP MESSAGE-----[.\n]*-----END PGP MESSAGE-----'; //[\n\w+\/=]

void showLinkPreview({String url, BuildContext context}) {
  showDialog(
      context: context,
      builder: (c) => AlertDialog(
            title: Text('Open url'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(url),
                SizedBox(
                    height: 96,
                    width: 400,
                    child: LinkPreviewer(
                      link: url,
                      borderColor: Colors.transparent,
                    )),
              ],
            ),
            actions: [
              FlatButton(
                child: Text('Cancel'),
                onPressed: Navigator.of(context).pop,
              ),
              FlatButton(
                child: Text('Open'),
                onPressed: () {
                  launch(url);
                  Navigator.of(context).pop();
                },
              )
            ],
          ));
}

bool isHtmlMessage(MimeMessage message) {
  bool isHtml = false;
  try {
    String html = message.decodeTextHtmlPart();
    if (html?.trim()?.isNotEmpty ?? false) isHtml = true;
  } catch (e) {}
  return isHtml;
}

class PGPMetadata {
  bool isEncrypted;
  bool isSigned;
  bool hasPublicKey;
  bool hasSecretKey;

  PGPMetadata(
      {this.isEncrypted = false,
      this.isSigned = false,
      this.hasPublicKey = false,
      this.hasSecretKey = false});
}
