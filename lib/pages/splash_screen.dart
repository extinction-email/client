import 'package:client/pages/inbox_page.dart';
import 'package:client/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = '/';
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    checkLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Hero(
              tag: 'logo',
              child: Image.asset(
                'assets/icons/icon-white-android.png',
                scale: 2,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(32.0),
              child: CircularProgressIndicator(),
            )
          ]),
    ));
  }

  Future<void> checkLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('user') &&
        prefs.containsKey('password') &&
        prefs.containsKey('private_keys')) {
      Navigator.of(context).pushReplacementNamed(InboxPage.routeName);
    } else {
      Navigator.of(context).pushReplacementNamed(LoginPage.routeName);
    }
  }
}
