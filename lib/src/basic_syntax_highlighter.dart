// This is a dummy implementation for Syntax highlighter.
import 'package:flutter/material.dart';

/// Ideally, you would implement the `SyntaxHighlighterBase` interface as per your need of highlighting rules.
class BasicSyntaxHighlighter {
  List<TextSpan> parseText(String text) {
    var texts = text.split(' ');

    var lsSpans = <TextSpan>[];
    texts.forEach((text) {
      if (text == 'class') {
        lsSpans
            .add(TextSpan(text: text, style: TextStyle(color: Colors.green)));
      } else if (text == 'if' || text == 'else') {
        lsSpans.add(TextSpan(text: text, style: TextStyle(color: Colors.blue)));
      } else if (text == 'return') {
        lsSpans.add(TextSpan(text: text, style: TextStyle(color: Colors.red)));
      } else {
        lsSpans
            .add(TextSpan(text: text, style: TextStyle(color: Colors.black)));
      }
      lsSpans.add(TextSpan(text: ' ', style: TextStyle(color: Colors.black)));
    });
    return lsSpans;
  }
}
